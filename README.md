# The haxxors relay

A relatively simple Python 3.8+ chat relay bot that has rudimentary support
for replies and reactions. See relay.example.toml for an example configuration.
