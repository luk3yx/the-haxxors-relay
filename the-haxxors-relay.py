#!/usr/bin/python3
#
# the-haxxors-relay: A primitive-ish IRC, Discord, and Matrix relay bot
# Copyright © 2023 by luk3yx
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from __future__ import annotations
import collections, miniirc, random, re, sys, threading, uuid  # type: ignore
from collections.abc import Hashable, Mapping, Sequence
from concurrent.futures import ThreadPoolExecutor
from typing import Any, TypeVar, Optional, TYPE_CHECKING

if sys.version_info >= (3, 11):
    import tomllib
else:
    import tomli as tomllib


_KT = TypeVar('_KT')
_VT = TypeVar('_VT')
if sys.version_info >= (3, 9) or TYPE_CHECKING:
    from collections import OrderedDict as _OrderedDict
else:
    class _OrderedDict(collections.OrderedDict):
        def __class_getitem__(cls, item):
            return cls


class LRUMap(_OrderedDict[_KT, _VT]):
    __slots__ = ()

    def add(self, key: _KT, value: _VT) -> None:
        while len(self) >= 500:
            self.popitem(last=False)
        self[key] = value

    def find_key(self, value: _VT) -> Optional[_KT]:
        for k, v in self.items():
            if v == value:
                return k
        return None


# Used to remove characters that may be stripped or replaced server-side
INVISIBLE_CHARS = '\u200b\u200c\u200d\u202c\ufeff'
_msg_re = re.compile(
    r'\x02|\x1d|\x1f|\x1e|\x11|\x16|\x0f'
    r'|\x03([0-9]{1,2})?(?:,([0-9]{1,2}))?'
    r'|\x04([0-9a-fA-F]{6})?(?:,([0-9a-fA-F]{6}))?'
    r'|[^\w' + re.escape(INVISIBLE_CHARS) + r':,\.]'
)

# Discord mentions
_mention_re = re.compile(r'(?<!\S)@(\S+)')
_emoji_re = re.compile(r'<a?(:\w+:)[0-9]+>')
_discord_mention_re = re.compile(r'<@([0-9]+)>')

NICK_COLOURS = (3, 4, 5, 6, 52, 53, 57, 58, 59, 72, 42)
_action_re = re.compile(r'\x01ACTION(?: (.*?))\x01?$', re.IGNORECASE)


def _casefold_channel(channel: str) -> str:
    return channel if channel.startswith('!') else channel.lower()


class Relay:
    def __init__(self, irc: miniirc.IRC,
                 channel_to_global: Mapping[str, Hashable],
                 relays: Sequence[Relay], username_format: Optional[str],
                 other_relay_bots: Mapping[str, Optional[str]],
                 ignored_nicks: Optional[Sequence[str]]) -> None:
        self.irc = irc
        irc.ircv3_caps.update(('echo-message', 'labeled-response', 'batch',
                               'message-tags', 'invite-notify'))
        irc.Handler('PRIVMSG', colon=False, ircv3=True)(self.on_privmsg)
        irc.Handler('TAGMSG', colon=False, ircv3=True)(self.on_tagmsg)
        irc.Handler('JOIN', colon=False, ircv3=True)(self.on_join)
        irc.Handler('PART', colon=False, ircv3=True)(self.on_part)
        irc.Handler('KICK', colon=False, ircv3=True)(self.on_kick)
        irc.Handler('INVITE', colon=False)(self.on_invite)

        self._channel_to_global = {_casefold_channel(k): v
                                   for k, v in channel_to_global.items()}
        self._global_to_channel = {v: k for k, v in channel_to_global.items()}
        self._relays = relays
        self.ignored_nicks = frozenset(map(str.lower, ignored_nicks or ()))
        self.username_format = username_format
        self.other_relay_bots = other_relay_bots

        # Maps remote message IDs to local ones
        self._msgid_map: LRUMap[uuid.UUID, str] = LRUMap()

        self._outgoing_msgs: LRUMap[str, uuid.UUID] = LRUMap()
        self._lock = threading.Lock()

    @classmethod
    def from_config(cls, irc: miniirc.IRC,
                    channel_to_global: Mapping[str, Hashable],
                    relays: Sequence[Relay], conf: Mapping[str, Any]) -> Relay:
        return cls(
            irc, channel_to_global, relays, conf.get('username_format'),

            {bot_info['nick'].lower(): bot_info.get('account')
             for bot_info in conf.get('other_relay_bot', ())},

            conf.get('ignored_nicks', ()),
        )

    def send_msg(self, global_channel: Hashable, msgid: uuid.UUID, msg: str, *,
                 reply: Optional[uuid.UUID]) -> None:
        channel = self._global_to_channel.get(global_channel)
        if not channel:
            return

        # Send the message
        tags = {}

        if 'labeled-response' in self.irc.active_caps:
            tags['label'] = f'm:{msgid.hex}'
        elif (self.irc.connected and 'echo-message' in self.irc.active_caps and
                'message-tags' in self.irc.active_caps):
            # Fallback: Store the message content temporarily
            with self._lock:
                canonical_msg = _msg_re.sub('', msg)
                # Make sure the message is unambiguous by adding invisible
                # characters if required
                while canonical_msg in self._outgoing_msgs:
                    char = random.choice(INVISIBLE_CHARS)
                    canonical_msg += char
                    msg += char
                self._outgoing_msgs[canonical_msg] = msgid
                # print(repr(msg), '->', repr(canonical_msg))

        # Add a reply (if required)
        if reply and (reply_id := self._msgid_map.get(reply)):
            tags['+draft/reply'] = reply_id
        # print('Incoming reply', reply, '->',
        #       reply and self._msgid_map.get(reply, 'N/A'))

        self.irc.msg(channel, msg, tags=tags)

    def send_reaction(self, global_channel: Hashable, reply: uuid.UUID,
                      reaction: str) -> None:
        channel = self._global_to_channel.get(global_channel)
        if not channel or not (reply_id := self._msgid_map.get(reply)):
            return

        self.irc.send('TAGMSG', channel, tags={'+draft/reply': reply_id,
                                               '+draft/react': reaction})

    def get_display_name(self, hostmask: tuple[str, str, str]) -> str:
        return hostmask[0]

    def colourise_nick(self, nick: str) -> str:
        index = ord(nick[0]) + len(nick)
        if self.username_format:
            nick = self.username_format.format(nick)
        if nick.startswith(','):
            nick = ',99' + nick
        return f'\x03{NICK_COLOURS[index % len(NICK_COLOURS)]:02}{nick}\x0f'

    def _relay(self, local_channel: str, msg: str,
               tags: Optional[Mapping[str, str]] = None) -> None:
        global_channel = self._channel_to_global.get(
            _casefold_channel(local_channel)
        )
        if global_channel is None:
            print(f'Got message in unknown channel: {local_channel!r}')
            return

        global_id = uuid.uuid4()
        reply: Optional[uuid.UUID] = None

        if tags is not None:
            # Store the local message ID
            if local_id := tags.get('msgid'):
                self._msgid_map.add(global_id, local_id)

            # Get the message that this is in reply to
            if local_reply_id := tags.get('+draft/reply'):
                reply = self._msgid_map.find_key(local_reply_id)
            # print('Reply', local_reply_id, '->', reply)

        for relay in self._relays:
            if relay is not self:
                relay.send_msg(global_channel, global_id, msg, reply=reply)

    def on_privmsg(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                   tags: Mapping[str, str], args: Sequence[str]) -> None:
        if hostmask[0] == irc.current_nick:
            # Store the message IDs of outgoing messages
            if label := tags.get('label'):
                # labeled-response: More reliable and less hacky
                if label.startswith('m:') and 'msgid' in tags:
                    self._msgid_map.add(uuid.UUID(label[2:]), tags['msgid'])
            elif self._outgoing_msgs:
                # No labeled-response: Search for the message in _outgoing_msgs
                c_msg = _msg_re.sub('', args[1])
                if ((global_id := self._outgoing_msgs.pop(c_msg, None)) and
                        'msgid' in tags):
                    # Fallback for IRC servers that don't support it
                    self._msgid_map.add(global_id, tags['msgid'])
            return

        lowercase_nick = hostmask[0].lower()
        if lowercase_nick in self.ignored_nicks:
            return

        if lowercase_nick in self.other_relay_bots:
            # Relay messages from other relay bots without any prefixing
            account = self.other_relay_bots[lowercase_nick]
            if not account or tags.get('account') == account:
                self._relay(args[0], args[1], tags)
                return

        # Relay the message
        name = self.colourise_nick(self.get_display_name(hostmask))
        if match := _action_re.match(args[1]):
            self._relay(args[0], f'* {name} {match.group(1)}', tags)
        else:
            self._relay(args[0], f'<{name}> {args[1]}', tags)

    def on_tagmsg(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                  tags: Mapping[str, str], args: Sequence[str]) -> None:
        if (hostmask[0] == irc.current_nick or
                hostmask[0].lower() in self.ignored_nicks):
            return

        # Make sure that the TAGMSG is a reaction
        global_channel = self._channel_to_global.get(
            _casefold_channel(args[0])
        )
        if global_channel is None:
            return
        reply: Optional[uuid.UUID] = None
        if local_reply_id := tags.get('+draft/reply'):
            reply = self._msgid_map.find_key(local_reply_id)
        if not reply or not isinstance(tags.get('+draft/react'), str):
            return

        # Send the reaction
        for relay in self._relays:
            if relay is not self:
                relay.send_reaction(global_channel, reply,
                                    tags['+draft/react'])

    def on_join(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                tags: Mapping[str, str], args: Sequence[str]) -> None:
        if (hostmask[0] == irc.current_nick or
                hostmask[0].lower() in self.ignored_nicks):
            return
        name = self.colourise_nick(self.get_display_name(hostmask))
        self._relay(args[0], f'\x039--> {name}\x039 has joined the channel',
                    tags)

    def on_part(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                tags: Mapping[str, str], args: Sequence[str]) -> None:
        if hostmask[0].lower() in self.ignored_nicks:
            return
        name = self.colourise_nick(self.get_display_name(hostmask))
        self._relay(args[0], f'\x034<-- {name}\x034 has left the channel',
                    tags)

    def on_kick(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                tags: Mapping[str, str], args: Sequence[str]) -> None:
        if args[1].lower() in self.ignored_nicks:
            return
        name = self.colourise_nick(self.get_display_name(hostmask))
        victim = self.colourise_nick(args[1])
        self._relay(args[0], f'\x034<-- {victim}\x034 has been kicked by '
                    f'{name} ({args[-1]})', tags)

    def on_invite(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                  args: Sequence[str]) -> None:
        # Auto-accept invites to any relayed channels
        if args[0] == irc.current_nick:
            channel = _casefold_channel(args[1])
            if channel not in self._channel_to_global:
                return
            # Send the correct case of the channel name in the JOIN command
            # in case the casefolded channel is (somehow) different
            global_channel = self._channel_to_global[channel]
            irc.send('JOIN', self._global_to_channel[global_channel])
        else:
            # Relay invites to other users
            name = self.colourise_nick(self.get_display_name(hostmask))
            self._relay(args[1], f'-- {name} has invited '
                        f'{self.colourise_nick(args[0])} to the channel')


class MatrixRelay(Relay):
    """ A relay with Matrix extensions """

    def get_display_name(self, hostmask: tuple[str, str, str]) -> str:
        return hostmask[0].lstrip('@')


class DiscordRelay(Relay):
    """ A Relay with Discord extensions """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._name_to_mention: LRUMap[str, str] = LRUMap()

    def _mention_repl(self, match: re.Match[str]) -> str:
        return self._name_to_mention.get(match.group(1), match.group(0))

    def send_msg(self, global_channel: Hashable, msgid: uuid.UUID, msg: str, *,
                 reply: Optional[uuid.UUID]) -> None:
        # Transform @mentions into real mentions
        msg = _mention_re.sub(self._mention_repl, msg)
        super().send_msg(global_channel, msgid, msg, reply=reply)

    def get_display_name(self, hostmask: tuple[str, str, str]) -> str:
        return hostmask[1].lstrip('@').rsplit('#', 1)[0]

    def _discord_mention_repl(self, match: re.Match[str]) -> str:
        orig_mention = match.group(1)
        mentions = {f'<@{orig_mention}>', f'<@!{orig_mention}>'}
        for name, mention in self._name_to_mention.items():
            if mention in mentions:
                return '@' + name
        return match.group(0)

    def on_privmsg(self, irc: miniirc.IRC, hostmask: tuple[str, str, str],
                   tags: Mapping[str, str], args: Sequence[str]) -> None:
        # Collect Discord nicknames
        self._name_to_mention[self.get_display_name(hostmask)] = hostmask[0]

        # Remove emoji syntax
        msg = _emoji_re.sub(r'\1', args[1])

        # Parse mentions
        msg = _discord_mention_re.sub(self._discord_mention_repl, msg)

        super().on_privmsg(irc, hostmask, tags, [args[0], msg])


class ConfigError(ValueError):
    pass


def parse_config(config: dict[str, Any]) -> list[Relay]:
    # Parse the channels
    networks: dict[str, dict[str, Hashable]] = collections.defaultdict(dict)
    for i, channel_info in enumerate(config['channel']):
        for network_name, local_chan in channel_info.items():
            networks[network_name][local_chan] = i

    irc = config.get('irc', {})
    matrix = config.get('matrix', {})
    discord = config.get('discord', {})

    # Allow nameless networks
    irc.setdefault('irc', irc)
    matrix.setdefault('matrix', matrix)
    discord.setdefault('discord', discord)

    executor = ThreadPoolExecutor()

    # Find networks
    relays: list[Relay] = []
    for network_name, channel_to_global in networks.items():
        if info := irc.get(network_name):
            relays.append(Relay.from_config(
                miniirc.IRC(info['ip'], info.get('port', 6697), info['nick'],
                            channel_to_global.keys(),
                            ssl=info.get('ssl', True), ident=info.get('ident'),
                            realname=info.get('realname'),
                            ns_identity=info.get('ns_identity'),
                            auto_connect=False, executor=executor),
                channel_to_global, relays, info
            ))
        elif info := matrix.get(network_name):
            import miniirc_matrix  # type: ignore
            kwargs = {}
            for arg in 'media_proxy_port', 'media_proxy_url', 'ssl':
                if arg in info:
                    kwargs[arg] = info[arg]

            relays.append(MatrixRelay.from_config(
                miniirc_matrix.Matrix(info['homeserver'], token=info['token'],
                                      auto_connect=False, executor=executor,
                                      **kwargs),
                channel_to_global, relays, info
            ))
        elif info := discord.get(network_name):
            import miniirc_discord  # type: ignore
            relays.append(DiscordRelay.from_config(
                miniirc_discord.Discord(info['token'], auto_connect=False,
                                        executor=executor),
                channel_to_global, relays, info
            ))
        else:
            raise ConfigError(f'Could not find configuration for network '
                              f'{network_name!r}')

    if not relays:
        raise ConfigError('No networks configured!')

    return relays


def main_loop(relays: list[Relay]) -> None:
    for relay in relays:
        relay.irc.connect()
    for relay in relays:
        relay.irc.wait_until_disconnected()


def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('config_file', help='The configuration file to use.')
    args = parser.parse_args()
    with open(args.config_file, 'rb') as f:
        config = tomllib.load(f)

    try:
        relays = parse_config(config)
    except ConfigError as exc:
        print(f'Error loading configuration file: {exc}', file=sys.stderr)
        sys.exit(1)

    del argparse, parser, args, config, f
    main_loop(relays)


if __name__ == '__main__':
    main()
